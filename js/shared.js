// Shared code needed by the code of all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";
var REGION_LIST_KEY = APP_PREFIX + ".RegionList"
var SETTINGS_KEY = APP_PREFIX + ".Settings"
var DEFAULT_MAX_FENCE_POSTS_DISTANCE = 4;

let geod = GeographicLib.Geodesic.WGS84;

// Array of saved Region objects.
var savedRegions = [];

class Region {
    constructor(regionName, locations = []) {
        this._locations = locations;
        this._savedDateTime = new Date();
        this._regionId = regionName;
    }

    getLocations() {
        return this._locations;
    }

    getArea() {
        let p = geod.Polygon();
        this._locations.forEach(point => {
            p.AddPoint(point[1], point[0]);
        });
        return p.Compute(true, true).area.toFixed(2);
    }


    getPerimeter() {
        let p = geod.Polygon();
        this._locations.forEach(point => {
            p.AddPoint(point[1], point[0]);
        });
        return p.Compute(true, true).perimeter.toFixed(2) + " m";
    }

    getFencePostLocation(maxFencePostDistance) {
        let fencePostLocations = [];
        for (let i = 0; i < this._locations.length - 1; i++) {
            let result = geod.Inverse(this._locations[i][1], this._locations[i][0], this._locations[i + 1][1], this._locations[i + 1][0]);

            let nextPoint = geod.Direct(this._locations[i][1], this._locations[i][0], result.azi1, maxFencePostDistance);
            // console.log(result);
            let lengthOfFence = 0;
            while (lengthOfFence < result.s12 - maxFencePostDistance) {

                // console.log(nextPoint);
                fencePostLocations.push([nextPoint.lon2, nextPoint.lat2]);
                lengthOfFence += parseFloat(maxFencePostDistance);
                nextPoint = geod.Direct(nextPoint.lat2, nextPoint.lon2, result.azi1, maxFencePostDistance);
                // console.log("Fence " + lengthOfFence);
            }
        }
        // console.log(fencePostLocations);
        return fencePostLocations;
    }

    fromData(data) {
        this._locations = data._locations;
        this._savedDateTime = data._savedDateTime;
        this._regionId = data._regionId;
    }
}


class RegionList {
    constructor() {
        this._regions = [];
        this._numberOfRegions = 0;
    }

    addRegion(region) {
        this._regions.push(region);
    }

    getRegion(id) {
        return this._regions.find(r => r._regionId == id);
    }

    removeRegion(id) {
        this._regions.slice(a => a._regionId == id, 1);
    }

    getNumberOfRegions() {
        return this._regions.length;
    }

    getAllRegions() {
        return this._regions;
    }

    getRegionAtIndex(index) {
        return this._regions[index];
    }

    removeRegionAtIndex(index) {
        this._regions.splice(index, 1);
    }

    updateNumberOfRegions() {
        this._numberOfRegions = this._regions.length;
    }

    fromData(data) {
        // console.log("test --------" + storage._regions[0]._regionId);
        this._regions = [];
        for (let i = 0; i < data._regions.length; i++) {
            let region = new Region();
            region.fromData(data._regions[i]);
            // console.log("region " + region._regionId);
            this._regions.push(region);
        }
        this._numberOfRegions = this._regions.length;
    }
}

//Shared functions
function getAllRegionNames(regionList) {
    let names = regionList.getAllRegions().map(element => element._regionId);
    console.log(names);
    return names;
}

//Initialize region list in local storage

let regionList = new RegionList();

if (localStorage.getItem(REGION_LIST_KEY) == null) {
    localStorage.setItem(REGION_LIST_KEY, JSON.stringify(regionList));
} else {
    regionList.fromData(JSON.parse(localStorage.getItem(REGION_LIST_KEY)));
}


let settings = {
    maxFencePostDistance: DEFAULT_MAX_FENCE_POSTS_DISTANCE
};

if (localStorage.getItem(SETTINGS_KEY) == null) {
    localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings));
} else {
    settings = JSON.parse(localStorage.getItem(SETTINGS_KEY));
}



//  getAllRegionNames(regionList);




