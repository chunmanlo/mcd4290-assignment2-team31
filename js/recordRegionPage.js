// Code for the Record Region page.
"use strict";

mapboxgl.accessToken = "pk.eyJ1IjoiY2h1bm1hbmxvIiwiYSI6ImNrajltZXp2OTFmeWcyc255czFla2VrbHYifQ.VrBdCNkpaRo96KGRL_YsFg";

let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 18,
    center: [145.11955, -37.92621]
});

//Get user's current location Ref:https://stackoverflow.com/questions/54405968/how-to-get-the-current-location-of-the-user
function goToCurrentLocation() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
            map.flyTo({
                center: [position.coords.longitude, position.coords.latitude],
                essential: true
            })

            let popup = new mapboxgl.Popup({ closeOnClick: false })
                .setLngLat([position.coords.longitude, position.coords.latitude])
                .setText("Your current location")
                .addTo(map);

            new mapboxgl.Marker()
                .setLngLat([position.coords.longitude, position.coords.latitude])
                .setPopup(popup)
                .addTo(map);
        });
    }
    //geolocation IS NOT available, or user doesn't allow to get their current location
    else {
        alert("Loaction permission is required to use this feature.")
    }
}


let corners = [];

let geojson = {
    type: "FeatureCollection",
    features: []
};

// Used to draw a line between points
let linestring = {
    type: "Feature",
    geometry: {
        type: "LineString",
        coordinates: []
    }
};

map.on("load", function () {
    map.addSource("geojson", {
        type: "geojson",
        data: geojson
    });

    map.addLayer({
        id: "measure-points",
        type: "circle",
        source: "geojson",
        paint: {
            "circle-radius": 5,
            "circle-color": "#000"
        },
        filter: ["in", "$type", "Point"]
    });

    map.addLayer({
        id: "measure-lines",
        type: "line",
        source: "geojson",
        layout: {
            "line-cap": "round",
            "line-join": "round"
        },
        paint: {
            "line-color": "#000",
            "line-width": 2.5
        },
        filter: ["in", "$type", "LineString"]
    });

    map.on("click", function (e) {
        // var features = map.queryRenderedFeatures(e.point, {
        //     layers: ["measure-points"]
        // });

        if (geojson.features.length > 1) {
            geojson.features.pop();
        }

        var point = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [e.lngLat.lng, e.lngLat.lat]
            },
            properties: {
                id: String(new Date().getTime())
            }
        };

        if (geojson.features.length == 0) {
            //Initial point
            geojson.features.push(point);
            //Destination point
            geojson.features.push(point);
        } else {
            geojson.features.splice(geojson.features.length - 1, 0, point);
        }

        linestring.geometry.coordinates = geojson.features.map(function (point) {
            return point.geometry.coordinates;
        });
        console.log("test--------------" + linestring.geometry.coordinates);
        //show path
        geojson.features.push(linestring);

        corners = linestring.geometry.coordinates;

        // console.log(corners);

        map.getSource("geojson").setData(geojson);
    });
});


document.getElementById("btnResetRegion").addEventListener("click", function (e) {

    geojson.features = [];

    corners = [];

    // console.log(geojson);

    map.getSource("geojson").setData(geojson);

});

document.getElementById("btnCreateRegion").addEventListener("click", function (e) {
    if (corners.length < 4) {
        alert("Please select at least 3 corners.");
    } else {
        let regionName = prompt("Please enter a name for this region: ");

        if (regionName != null && regionName.trim() != "") {
            if (!getAllRegionNames(regionList).includes(regionName.trim())) {

                let region = new Region(regionName, corners);
                regionList.addRegion(region);
                regionList.updateNumberOfRegions();

                localStorage.setItem(REGION_LIST_KEY, JSON.stringify(regionList));

                window.location = "index.html";

            } else {
                alert("Region name already exists. Please enter a different one.");
            }

        } else {
            alert("Region name cannot be empty.")
        }
    }



});

document.getElementById("btnCurrentLocation").addEventListener("click", function (e) {

    goToCurrentLocation();

});

//Functions run on page load
goToCurrentLocation();



