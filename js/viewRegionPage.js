// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
mapboxgl.accessToken = "pk.eyJ1IjoiY2h1bm1hbmxvIiwiYSI6ImNrajltZXp2OTFmeWcyc255czFla2VrbHYifQ.VrBdCNkpaRo96KGRL_YsFg";

var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion");
let selectedRegion = regionList.getRegionAtIndex(regionIndex);

if (regionIndex !== null) {
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    // var regionNames = ["Region A", "Region B"];
    // document.getElementById("headerBarTitle").textContent = regionNames[regionIndex];

    document.getElementById("headerBarTitle").textContent = "Region " + selectedRegion._regionId;
}

let map = new mapboxgl.Map({
    container: "map1",
    style: "mapbox://styles/mapbox/streets-v11",
    zoom: 18,
    center: [145.11955, -37.92621]
});



let geojson = {
    type: "FeatureCollection",
    features: []
};


let geojsonWithoutFencePosts = {
    type: "FeatureCollection",
    features: []
};

// Used to draw a line between points
let linestring = {
    type: "Feature",
    geometry: {
        type: "LineString",
        coordinates: []
    }
};

let polygon = {
    type: "Feature",
    geometry: {
        type: "Polygon",
        coordinates: []
    }
};

map.on("load", function () {
    map.addSource("geojson", {
        type: "geojson",
        data: geojson
    });

    map.addLayer({
        id: "measure-points",
        type: "circle",
        source: "geojson",
        paint: {
            "circle-radius": 5,
            "circle-color": "#000"
        },
        filter: ["in", "$type", "Point"]
    });

    map.addLayer({
        id: "measure-lines",
        type: "line",
        source: "geojson",
        layout: {
            "line-cap": "round",
            "line-join": "round"
        },
        paint: {
            "line-color": "#000",
            "line-width": 2.5
        },
        filter: ["in", "$type", "LineString"]
    });

    map.addLayer({
        'id': 'region-boundary',
        'type': 'fill',
        'source': 'geojson',
        'paint': {
            'fill-color': '#888888',
            'fill-opacity': 0.4
        },
        'filter': ['==', '$type', 'Polygon']
    });

    for (let i = 0; i < selectedRegion.getLocations().length; i++) {
        let location = selectedRegion.getLocations()[i];
        var point = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [location[0], location[1]]
            },
            properties: {
                id: String(new Date().getTime())
            }
        };
        geojson.features.push(point);
    }

    linestring.geometry.coordinates = geojson.features.map(function (point) {
        return point.geometry.coordinates;
    });
    geojson.features.push(linestring);

    polygon.geometry.coordinates = geojson.features.map(function (point) {
        return point.geometry.coordinates;
    });
    geojson.features.push(polygon);

    map.flyTo({
        center: [selectedRegion._locations[0][0], selectedRegion._locations[0][1]],
        essential: true
    })

    geojsonWithoutFencePosts = JSON.parse(JSON.stringify(geojson))

    if (document.getElementById("showFencePosts").checked) {
        let fencePostsLocations = selectedRegion.getFencePostLocation(settings.maxFencePostDistance);

        for (let i = 0; i < fencePostsLocations.length; i++) {
            let location = fencePostsLocations[i];
            var point = {
                type: "Feature",
                geometry: {
                    type: "Point",
                    coordinates: [fencePostsLocations[i][0], fencePostsLocations[i][1]]
                },
                properties: {
                    id: String(new Date().getTime())
                }
            };
            geojson.features.push(point);
        }
    }
    map.getSource("geojson").setData(geojson);
});



document.getElementById("showFencePosts").addEventListener("click", function (e) {

    if (document.getElementById("showFencePosts").checked) {
        // console.log(geojson);
        map.getSource("geojson").setData(geojson);
    } else {
        // console.log(geojsonWithoutFencePosts);
        map.getSource("geojson").setData(geojsonWithoutFencePosts);
    }
});

document.getElementById("btnRemoveRegion").addEventListener("click", function (e) {

    if (confirm("Are you sure you want to remove this region?")) {

        regionList.removeRegionAtIndex(regionIndex);
        regionList.updateNumberOfRegions();
        localStorage.setItem(REGION_LIST_KEY, JSON.stringify(regionList));
        window.location = "index.html";
    }
});



window.onload = function (e) {
    document.getElementById("perimeterValue").innerText = selectedRegion.getPerimeter();
    document.getElementById("areaValue").innerText = selectedRegion.getArea();
}

