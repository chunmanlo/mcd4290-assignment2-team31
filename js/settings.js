document.getElementById("btnUpdateSetting").addEventListener("click", function (e) {

    // let distance = document.getElementById("fencePostsDistance").value;

    settings.maxFencePostDistance = document.getElementById("fencePostsDistance").value;
    localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings));

});

document.getElementById("btnDefaultSetting").addEventListener("click", function (e) {

    document.getElementById("fencePostsDistance").value = DEFAULT_MAX_FENCE_POSTS_DISTANCE;

    settings.maxFencePostDistance = DEFAULT_MAX_FENCE_POSTS_DISTANCE;
    localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings));

});


window.onload = function () {
    document.getElementById("fencePostsDistance").value = settings.maxFencePostDistance;
}