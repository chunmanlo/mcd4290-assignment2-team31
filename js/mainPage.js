// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRegion(regionIndex) {
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex);
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}


function displayRegions(regisonList) {
    let html = "";

    console.log(regionList.getNumberOfRegions());

    for (i = 0; i < regionList.getNumberOfRegions(); i++) {

        let region = regionList.getRegionAtIndex(i);
        html = html + `
            <li class="mdl-list__item mdl-list__item--two-line" onclick="viewRegion(${i});">
                <span class="mdl-list__item-primary-content">
                <span>Region ${region._regionId}</span>
                <span class="mdl-list__item-sub-title">${region._savedDateTime}</span>
                </span>
            </li>
        
        `;
    }

    document.getElementById("regionsList").innerHTML = html;
}


//Functions called on page load

displayRegions(regionList);